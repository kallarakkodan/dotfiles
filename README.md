# dotfiles

My Config Files From Deepin
****************************

Install and Make zsh as default shell by following commands in terminal (For Debian,Ubuntu,Mint etc)



		sudo apt install zsh
	
To find out the path to zsh, enter the following in terminal.

		which zsh

Then enter the following and make zsh your default shell.

		chsh -s /bin/zsh root	(Making zsh the default shell)

Install GIT and WGET (skip if already installed)

		sudo apt install wget git

Now download the Oh My Zsh installer and run it

		wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh

Now oh-my-zsh is installed in the home directory '~/.oh-my-zsh'.

download and place .zshrc file from the repo to user's home directory

   Ex:
   
		cp .zshrc ~/

Download and install Source Code Pro For Powerline Font from the repo

Then change Terminal font to Source Code Pro For Powerline

install Autosuggestion and SyntaxHighlight plugins 

		git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions	

		git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting


Restart your terminal. you are all set now.


ENJOY.....!!!


